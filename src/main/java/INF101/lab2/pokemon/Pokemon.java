package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;




public class Pokemon implements IPokemon {

    //--------------------- Field Variables
    String name; //name of Pokemon
    int healthPoints; //HP
    int maxHealthPoints; //Maximum HP
    int strength; //Strength of Pokemon
    Random random; //Randomizer 
    //--------------------- Field Variables
    
    
    //--------------------- Constructors 
    public Pokemon(String name) {

        this.name = name; //this. takes the variable - alternatively use "n" as the variable and "name = n"
        this.random = new Random();       
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));

    }
    //--------------------- Constructors 

    public String getName() {
        return name;
        
    }

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public int getCurrentHP() {
        return healthPoints;
    }

    @Override
    public int getMaxHP() {
        return maxHealthPoints;
    }

    public boolean isAlive() {
        if (healthPoints > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    // Attack this.target and calculate damage HP using strength and a random element.  
    public void attack(IPokemon target) {
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian()); //randomize damage
        System.out.println(this.getName() + " attacks " + target.getName() + ".");
        
        target.damage(damageInflicted); //inflit damage to target
            if (target.isAlive() == false) { // check if alive, if not alive print:
                System.out.println(target.getName() + " is defeated by " + this.getName() + "."); 
    }}

    @Override
    // calculate new HP after damage and prevent HP from going below 0
    public void damage(int damageTaken) {
        if (damageTaken < 0) {
            damageTaken = 0; //cannot inflict negative damage
        }
        int damagedHealthPoints = (int) (healthPoints - damageTaken); //subtract damage from HP
            if (damagedHealthPoints < 0) { // prevent new HP to go below 0
                damagedHealthPoints = 0;   // if new HP is below 0, new HP = 0
        }

        healthPoints = damagedHealthPoints; //save new HP as damagedHealthPoints
        

        // Print out "Pikachu takes 3 damage and is left with 84/94 HP"
        System.out.println(name + " takes " + damageTaken + " damage and is left with " + damagedHealthPoints + "/" + maxHealthPoints + " HP.\r\n");
    }

    @Override
    //returns String "Pokemon name HP: (HP value/MaxHP value) STR: Strength value"
    public String toString() {
        return (name + " HP: " + "(" + healthPoints + "/" + maxHealthPoints + ")" + " STR: " + strength);
    }

}

